<img src="https://miro.medium.com/max/743/1*G1m1amDag587oPdKOkJSFw.png" width="800"  heigth="600">


<h3>DOCKER</h3>

Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package. By doing so, thanks to the container, the developer can rest assured that the application will run on any other Linux machine regardless of any customized settings that machine might have that could differ from the machine used for writing and testing the code.

Docker is open source. This means that anyone can contribute to Docker and extend it to meet their own needs if they need additional features that aren't available out of the box.

<img src="https://hackernoon.com/hn-images/1*lVUFswPZ96le-I7JjRWFhw.jpeg" width="800"  heigth="600">

<h3>Who is Docker for?</h3>

Docker is a tool that is designed to benefit both developers and system administrators, making it a part of many DevOps (developers + operations) toolchains. For developers, it means that they can focus on writing code without worrying about the system that it will ultimately be running on. It also allows them to get a head start by using one of thousands of programs already designed to run in a Docker container as a part of their application. For operations staff, Docker gives flexibility and potentially reduces the number of systems needed because of its small footprint and lower overhead.

<h3>Docker Containers</h3>
A container is the instance of an image. We can create, run, stop, or delete a container using the Docker CLI. We can connect a container to more than one networks, or even create a new image based on its current state.

By default, a container is well isolated from other containers and its system machine. A container defined by its image or configuration options that we provide during to create or run it.


<img src="https://docs.docker.com/engine/images/engine-components-flow.png" width="600"  heigth="600">


<h3>Docker Engine </h3>
The Docker engine is a part of Docker which create and run the Docker containers.  Docker Engine is a client-server based application with following components –

1. A server which is a continuously running service called a daemon process.
2. A REST API which interfaces the programs to use talk with the daemon and give instruct it what to do.
3. A command line interface client.

The command line interface client uses the Docker REST API to interact with the Docker daemon through using CLI commands. Many other Docker applications also use the API and CLI. The daemon process creates and manage Docker images, containers, networks, and volumes.
<img src="https://images.xenonstack.com/blog/Docker-Engine.jpg" width="700"  heigth="700">

<h3>The Docker Daemon</h3>

The Docker daemon is a service that runs on your host operating system. It currently only runs on Linux because it depends on a number of Linux kernel features, but there are a few ways to run Docker on MacOS and Windows too. TheDocker daemon itself exposes a REST API.
The docker daemon process is used to control and manage the containers. The Docker daemon listens to only Docker API requests and handles Docker images, containers, networks, and volumes. It also communicates with other daemons to manage Docker services.

<h3>Docker Client</h3>

Docker client is the primary service using which Docker users communicate with the Docker. When we use commands “docker run” the client sends these commands to dockerd, which execute them out.

The command used by docker depend on Docker API. In Docker, client can interact with more than one daemon process.

<h3>Docker Images</h3>
The Docker images are building the block of docker or docker image is a read-only template with instructions to create a Docker container. Docker images are the most build part of docker life cycle.

Mostly, an image is based on another image, with some additional customization in the image.
 
<h3>Docker Hub</h3>
 It is basically GitHub but for Docker Images.

 **Installing the Docker Engine:** [Official documentation: Installing Docker Engine](https://docs.docker.com/engine/install/ubuntu/)

<img src="https://miro.medium.com/max/875/1*ttU6oMoZztKk2kjJid6PuQ.png" width="800"  heigth="800">


#### Basic Docker Commands
***

**docker run** – Runs a command in a new container. **Syntax**: docker run -it -d <image name>

**docker start** – Starts one or more stopped containers

**docker stop** – Stops one or more running containers. **Syntax**: docker stop <container id>

**docker build** – Builds an image form a Docker file.  **Syntax**: docker build <path to docker file>

**docker pull** – Pulls an image or a repository from a registry. **Syntax**: docker pull <image name>

**docker push** – Pushes an image or a repository to a registry. **Syntax** :docker push <username/image name>

**docker export** – Exports a container’s filesystem as a tar archive

**docker exec** – Runs a command in a run-time container . **Syntax** :docker exec -it <container id> bash

**docker search** – Searches the Docker Hub for images

**docker attach** – Attaches to a running container

**docker commit** – Creates a new image from a container’s changes. **Syntax**: docker commit <conatainer id> <username/imagename>





