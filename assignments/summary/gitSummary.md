# GIT
Git is a version control (VCS) system for tracking changes to projects. Version control systems are also called revision control systems or source code management (SCM) systems.The aim of Git is to manage software development projects and its files, as they are changing over time. Git stores this information in a data structure called a repository. 

<img src="https://miro.medium.com/max/875/0*Q3t3BgVGSq7kWr5i.png" width="300"  heigth="300">

## Getting a Git Repository
You typically obtain a Git repository in one of two ways:

1. You can take a local directory that is currently not under version control, and turn it into a Git repository, or
2. You can clone an existing Git repository from elsewhere.

In either case, you end up with a Git repository on your local machine, ready for work.


## Basic Terminologies
**Repository**: It is a common space where everything related to a project is stored by every team member.

**Commit**: It is a method to save our work in our local repository.

**Push**: It is basically uploading our local repository onto internet.

**Branch**: Suppose we are working on a system having many subsystems, we create a branch to work specifcally on a particular subsystem.

**Merge**: When a Branch is free of errors and bugs, we merge it with the main project.

**Clone**: Cloning is making an exact copy of a GitLab account in our local system.

**Fork**: Forking is basically copying only the repository under our own username.


## States
Git has three main states that your files can reside in:
1. **Modified** means that you have changed the file but have not committed it to your database yet.
2. **Staged** means that you have marked a modified file in its current version to go into your next commit snapshot.
3. **Committed** means that the data is safely stored in your local database.

## Sections of a Git project
The three main sections of a Git project: 
1.  **The working tree**:The working tree is a single checkout of one version of the project. These files are pulled out of the compressed database in the Git directory and placed on disk for you to use or modify.
2. **The staging area**:The staging area is a file, generally contained in your Git directory, that stores information about what will go into your next commit. Its technical name in Git parlance is the “index”, but the phrase “staging area” works just as well.
3. **The Git directory**: The Git directory is where Git stores the metadata and object database for your project. This is the most important part of Git, and it is what is copied when you clone a repository from another computer.
<img src="https://git-scm.com/images/about/index1@2x.png" width="500"  heigth="500">


You modify files in your working tree.
You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Git directory.



## GitLab
GitLab is a Git-based repository manager and a powerful complete application for software development.GitLab allows you to work effectively, both from the command line and from the UI itself. It's not only useful for developers, but can also be integrated across your entire team to bring everyone into a single and unique platform.

<img src="https://www.philipp-doblhofer.at/wp-content/uploads/2019/11/gitlab_hero.png" width="500" height="200" >

## Gitlab Workflow
The GitLab Workflow is a logical sequence of possible actions to be taken during the entire lifecycle of the software development process, using GitLab as the platform that hosts your code.With the GitLab Workflow, the goal is to help teams work cohesively and effectively from the first stage of implementing something new (ideation) to the last stage—deploying implementation to production.
<img src="https://about.gitlab.com/images/blogimages/idea-to-production-10-steps.png" width="1000" height="200" >

## Git Commands
**git status**: check status and see what has changed

**git add**: add a changed file or a new file to be committed

**git diff**: see the changes between the current version of a file and the version of the file most recently committed

**git commit**: commit changes to the history

**git log**: show the history for a project

**git revert**: undo a change introduced by a specific commit

**git checkout**: switch branches or move within a branch

**git clone**: clone a remote repository

**git pull**: pull changes from a remote repoository

**git push**: push changes to a remote repository
